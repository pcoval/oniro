# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

# We support musl integration and the current status is stable for the scope of
# the project.
deltask warn_musl
